const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const AdminSchema = new Schema({

  users : [{
    user : {
      type : Schema.Types.ObjectId,
      ref  : 'User'
    }
  }],
  events : [{
    user :{
      type : Schema.Types.ObjectId,
      ref : 'User'
    }
  }],
  posts : [{
    user :{
      type : Schema.Types.ObjectId,
      ref : 'User'
    }
}],
likes : [{
  user :{
    type : Schema.Types.ObjectId,
    ref : 'User'
  }
}],
comments: [{
  user :{
    type : Schema.Types.ObjectId,
    ref : 'User'
  }
}],
interns: [{
  user : {
    type : Schema.Types.ObjectId,
    ref : 'Intern'
  }
}]

})

module.exports =  mongoose.model('Admin' , AdminSchema)