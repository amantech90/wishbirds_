const mongoose = require('mongoose');
const Schema  = mongoose.Schema;


const PostSchema = new Schema({
  
  owner : {
    type : Schema.Types.ObjectId,
    ref  : 'User'
  },
  name : {
    type : String,
    required : true
  },
  image : {
    type : String,
    required : true
  },
  wish : {
    type : String,
    required : true
  },
  email : {
    type : String,
    required : true
  },
  date: {
    type: String,
    required : true
  },
  event: {
    type : Schema.Types.ObjectId,
    ref : 'Event'
  },

})

module.exports =  mongoose.model('Post' , PostSchema)