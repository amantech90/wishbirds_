const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const EventSchema = new Schema({

  name : {
    type : String,
    required : true
  },
  avatar : {
    type : String,
  },
  desc : {
    type: String,
    required : true
  },
  owner : {
    type : Schema.Types.ObjectId,
    ref : 'User'
  },
  posts : [{
    user :{
      type : Schema.Types.ObjectId,
      ref : 'User'
    }
}],
date: {
  type: String,
  required : true
},
saveoption : {
  type : String
}


})

module.exports =  mongoose.model('Event' , EventSchema)