const mongoose = require('mongoose');

const Schema = mongoose.Schema;


const InternSchema = new Schema({
  name: {
    type: String,
  
  },
  share: {
    type: Number,
  
  },
  email: {
    type: String,
  
  },
  like: {
    type: Number
  },
points:{
  type : Number
},
rank:{
  type : Number
}
});

module.exports = Intern = mongoose.model('Intern', InternSchema);