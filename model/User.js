const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  avatar: {
    type: String
  },
  date: {
    type: Date,
    default: Date.now()
  },
  isAdmin : {
    type : String
  },
  isIntern : {
    type : String
  },
  resetPasswordToken: String ,
  resetPasswordExpires:  Date 
});

module.exports = User = mongoose.model('User', UserSchema);
