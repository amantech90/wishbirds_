const express = require('express');
const router = express.Router();
const User = require('../../model/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const sceret = require('../../config/db');
const validatorRegister = require('../../validator/registerValidator');
const validatorLogin = require('../../validator/loginValidator');
const validatorImage = require('../../validator/imageValidator');
const validatorChangePassword = require('../../validator/changePasswordValidator');
const validatornewPassword = require('../../validator/newPasswordValidator');
const validatorEmail = require('../../validator/emailValidator');
const cloudinaryDetails = require('../../config/db');
const cloudinary = require('cloudinary');
const crypto = require('crypto');
const async = require('async');
const nodemailer = require('nodemailer');
const Admin = require('../../model/Admin');
var schedule = require('node-schedule');
cloudinary.config({
  cloud_name: cloudinaryDetails.cloudName,
  api_key: cloudinaryDetails.api_key,
  api_secret: cloudinaryDetails.api_secret
});

router.post('/register', (req, res) => {
  const { errors, isValid } = validatorRegister(req.body);
  if (!isValid) {
    return res.status(400).json(errors);
  }
  User.findOne({ email: req.body.email }).then(user => {
    if (user) {
      return res.status(400).json({ email: 'email already exists' });
    } else {
      const newUser = new User({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        avatar: req.body.avatar
      });
      bcrypt.genSalt(16, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
          if (err) throw err;
          newUser.password = hash;
          newUser.adminpassword = hash;
          newUser
            .save()
            .then(user => {
              const payload = {
                name: user.name,
                id: user.id,
                avatar: user.avatar,
                email: user.email
              };

              jwt.sign(
                payload,
                sceret.secretKey,
                { expiresIn: 604800 },
                (err, token) => {
                  if (err) throw err;
                  res.json({
                    success: true,
                    token: 'Bearer ' + token
                  });
                }
              );
              Admin.findById(cloudinaryDetails.adminid).then(admin => {
                admin.users.unshift({ user: user.id });
                admin.save();
              });
            })
            .catch(err => console.log(err));
        });
      });
    }
  });
});

router.post('/login', (req, res) => {
  const { errors, isValid } = validatorLogin(req.body);
  if (!isValid) {
    return res.status(400).json(errors);
  }
  const email = req.body.email;
  const password = req.body.password;

  User.findOne({ email: email }).then(user => {
    if (!user) {
      errors.email = 'user is not found';
      return res.status(404).json(errors);
    } else {
      bcrypt.compare(password, user.password).then(isMatch => {
        if (isMatch) {
          //user match
          const payload = {
            name: user.name,
            id: user.id,
            avatar: user.avatar,
            email: user.email,
            isAdmin: user.isAdmin,
            isIntern: user.isIntern
          };
          //assign token
          jwt.sign(
            payload,
            sceret.secretKey,
            { expiresIn: 604800 },
            (err, token) => {
              if (err) throw err;
              res.json({
                success: true,
                token: 'Bearer ' + token
              });
            }
          );
        } else {
          errors.password = 'Oops ! your password is incorrect';
          return res.status(400).json(errors);
        }
      });
    }
  });
});

router.post(
  '/profile/change_password',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { errors, isValid } = validatorChangePassword(req.body);
    if (!isValid) {
      return res.status(400).json(errors);
    }
    User.findById(req.user.id).then(user => {
      bcrypt.compare(req.body.password, user.password).then(isMatch => {
        if (isMatch) {
          user.password = req.body.password1;
          bcrypt.genSalt(16, (err, salt) => {
            bcrypt.hash(user.password, salt, (err, hash) => {
              if (err) throw err;
              user.password = hash;
              user
                .save()
                .then(user => res.json({ user: user }))
                .catch(err => console.log(err));
            });
          });
        } else {
          errors.password = 'password is incorrect';
          return res.status(400).json(errors);
        }
      });
    });
  }
);

router.post(
  '/profile/updateprofilepic',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { errors, isValid } = validatorImage(req.body);
    if (!isValid) {
      return res.status(400).json(errors);
    }
    cloudinary.uploader.upload(req.body.image, function(result) {
      const img = result.public_id;
      User.findByIdAndUpdate(req.user.id, { avatar: img })
        .then(user => res.json(user))
        .catch(err => {
          errors.image =
            'Something is happen please try again or your internet speed is slow';
          res.status(400).json(errors);
        });
    });
  }
);

router.post('/forgotpassword', (req, res) => {
  async.waterfall([
    function(done) {
      crypto.randomBytes(20, (err, buf) => {
        let token = buf.toString('hex');
        done(err, token);
      });
    },
    function(token, done) {
      const { errors, isValid } = validatorEmail(req.body);
      if (!isValid) {
        return res.status(400).json(errors);
      }
      User.findOne({ email: req.body.email1 }, function(err, user) {
        if (!user) {
          errors.email1 = 'No user is found';
          return res.status(400).json(errors);
        } else {
          user.resetPasswordToken = token;
          user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

          user.save(function(err) {
            done(err, token, user);
          });
        }
      });
    },
    function(token, user, done) {
      let smtpTransport = nodemailer.createTransport({
        service: 'gmail',
        secure: 'false',
        port: '25',
        auth: {
          user: cloudinaryDetails.email,
          pass: cloudinaryDetails.password
        },
        tls: {
          rejectUnauthorized: false
        }
      });
      let mailOptions = {
        to: user.email,
        from: cloudinaryDetails.email,
        subject: 'Password Reset',
        text:
          'Hi ' +
          user.name +
          ' , you are receiving this because you have requested for reset the password for your account.\n\n' +
          'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
          'http://' +
          req.headers.host +
          '/forgot/reset/' +
          token +
          '\n\n' +
          'If you did not request this, please ignore this email and your password will remain unchanged.\n' +
          'This link is only valid for one hour'
      };


      smtpTransport.sendMail(mailOptions, function(err) {
        console.log('mail sent');
        done(err, 'done');
        res.json({ sent: 'Mail sent' });
      });
    }
  ]);
});

router.post('/forgot/reset/:token', (req, res) => {
  const { errors, isValid } = validatornewPassword(req.body);
  if (!isValid) {
    return res.status(400).json(errors);
  }
  User.findOne({
    resetPasswordToken: req.params.token,
    resetPasswordExpires: { $gt: Date.now() }
  })
    .then(user => {
      if (!user) {
        errors.msg = 'Token was expired , Please generate again';
        return res.status(400).json(errors);
      } else {
        bcrypt.genSalt(16, (err, salt) => {
          bcrypt.hash(req.body.password, salt, (err, hash) => {
            if (err) throw err;
            user.password = hash;
            user.resetPasswordExpires = undefined;
            user.resetPasswordToken = undefined;
            user
              .save()
              .then(user => {
                res
                  .status(200)
                  .json({ succes: 'Your password is changed successfully' });
              })
              .catch(err => {
                errors.msg = 'Token was expired , Please generate again';
                return res.status(400).json(errors);
              });
          });
        });
      }
    })
    .catch(err => {
      errors.msg = 'Token was expired , Please generate again';
      return res.status(400).json(errors);
    });
});

router.post('/sending' , (req , res)=>{
  let smtpTransport = nodemailer.createTransport({
    service: 'gmail',
    secure: 'false',
    port: '25',
    auth: {
      user: cloudinaryDetails.email,
      pass: cloudinaryDetails.password
    },
    tls: {
      rejectUnauthorized: false
    }
  });
  let mailOptions = {
    to: 'amantech90@gmail.com',
    from: cloudinaryDetails.email,
    subject: 'Password Reset',
    text:
      'Hi ' +
      "user.name" +
      ' , you are receiving this because you have requested for reset the password for your account.\n\n' +
      'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
      'http://' +
      req.headers.host +
      '/forgot/reset/' +
      "token" +
      '\n\n' +
      'If you did not request this, please ignore this email and your password will remain unchanged.\n' +
      'This link is only valid for one hour'
  };
  var date = req.body.date;
  console.log(date)
  var j = schedule.scheduleJob(date, function(){
    smtpTransport.sendMail(mailOptions, function(err) {
      console.log('mail sent');
      done(err, 'done');
      res.json({ sent: 'Mail sent' });
    });
  });


})

module.exports = router;7
