const express = require('express');
const router = express.Router();
const Event = require('../../model/Event');
const passport = require('passport');
const eventValidator = require('../../validator/eventValidator');
const cloudinaryDetails = require('../../config/db');
const Post = require('../../model/Post')
const Admin = require('../../model/Admin')



const cloudinary = require('cloudinary');
cloudinary.config({ 
  cloud_name: cloudinaryDetails.cloudName, 
  api_key: cloudinaryDetails.api_key, 
  api_secret: cloudinaryDetails.api_secret
});



router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { errors, isValid } = eventValidator(req.body);
    if (!isValid) {
      return res.status(400).json(errors);
    }

    cloudinary.uploader.upload(req.body.image, function(result) {
let now = new Date()
var options = { year: 'numeric', month: 'long', day: 'numeric' };
let str = now.toLocaleDateString('en-US' , options);
let time = now.toLocaleTimeString('en-US')
        const img = result.public_id;
        const newEvent = new Event({
          owner: req.user.id,
          name: req.body.name,
          avatar: img,
          desc: req.body.desc,
          saveoption : '',
          date : str
        });
        Event.findOne({ owner: req.user.id }).then(event => {
          if (event) {
            errors.msg = 'only one event can be created';
            res.status(400).json(errors);
          }
          newEvent.save().then(event => res.json(event));
        }).catch(err => res.status(400).json(err)  );
       Admin.findById(cloudinaryDetails.adminid).then(admin => {
         admin.events.unshift({user : req.user.id})
         admin.save()
       })
      


    })
  }
);

router.post('/update' , passport.authenticate('jwt' , {session : false}) , (req , res) => {
  Event.findOneAndUpdate({owner : req.user._id},{saveoption : req.body.saveoption}).then(event => {
    res.json(event)
  })
})

router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    Event.findOne({owner : req.user._id})
      .populate('owner', ['name', 'avatar'])
      .then(event => res.json(event))
      .catch(err => res.status(404).json({error : 'Oops! No event found'}));
  }
);

router.get(
  '/posts/:id',
  (req, res) => {
    Event.findById(req.params.id)
      .populate('owner', ['name', 'avatar'])
      .then(event => res.json(event))
      .catch(err => res.status(404).json({error: 'Oops! No event found'}));
  }
);

router.post(
  '/delete',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
   Event.findOne({owner : req.user._id}).then(event => {
        Post.findOneAndRemove({event : event._id}).then(res.json({success : true}))

        event.remove()
      })
});

module.exports = router;
