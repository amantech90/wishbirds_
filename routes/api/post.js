const express = require('express');
const router = express.Router();
const Post = require('../../model/Post');
const Event = require('../../model/Event');
const passport = require('passport');
const cloudinaryDetails = require('../../config/db');
const postvalidator = require('../../validator/postValidator')
const nodemailer = require('nodemailer');
let now = new Date();
var options = { year: 'numeric', month: 'long', day: 'numeric' };
let str = now.toLocaleDateString('en-US' , options)
const Admin = require('../../model/Admin')


const cloudinary = require('cloudinary');
cloudinary.config({ 
  cloud_name: cloudinaryDetails.cloudName, 
  api_key: cloudinaryDetails.api_key, 
  api_secret: cloudinaryDetails.api_secret
});

// api/posts/:id

router.get('/:id' , (req,res) => {
  Event.findById(req.params.id).then(foundEvent => {
      Post.find({event : req.params.id})
      .populate('owner' , ['name' , 'avatar'])
      .sort({ date: -1 })
      .then(posts => {
        res.json(posts)
      })
    
  }).catch(err => res.status(400).json({error : 'Link Has been broken or User deleted it'}))
})


router.post(
  '/:id',
  (req, res) => {
    const { errors, isValid } = postvalidator(req.body);
    if (!isValid) {
      return res.status(400).json(errors);
    }
   
    Event.findById(req.params.id).then(foundEvent => {
      if (foundEvent) {
        cloudinary.uploader.upload(req.body.image, function(result) {   
          const img = result.public_id;
        const newPost = new Post({
          name: req.body.name,
          email: req.body.email,
          image: img,
          wish: req.body.wish,
          event : req.params.id,
          date :  str
        });
        newPost.save().then(post => {
          Event.findByIdAndUpdate(req.params.id).then(event => {
            event.posts.unshift({user : post.id})
            event.save()
            Admin.findById(cloudinaryDetails.adminid).then(admin => {
              admin.posts.unshift({user : post.id})
              admin.save()
            })  
          })
          res.json(post)
        });
      })
      } else {
        errors.msg = 'Sorry The event has been deleted'
       return res.status(404).json(errors);
      }
    });
  }
);



router.delete('/:id' , passport.authenticate('jwt' , {session: false}) , (req , res) => {
  Event.findOne({owner : req.user.id}).then(foundEvent => {
    Post.findById(req.params.id).then(post => {
      if(foundEvent.owner.toString() !== req.user.id){
        res.status(401).json({notAuthenticated : `you know , you didn't created this post , so how can you delete this`}
)
console.log('You cant delete it')
      }else{
        post.remove().then(res.json({success : true}))
        const removeIndex = foundEvent.posts.map(post => 
          {post.user._id.toString()===req.user.id
          })

          foundEvent.posts.splice(removeIndex , 1)

          foundEvent.save().then({success : true})

      }
    }).catch(err => res.status(404).json({error : 'post not found'}))
  })
})

router.post('/thankyou/:id' , passport.authenticate('jwt' , {session : false}) , (req , res) => {
  Post.findById(req.params.id).then(foundPost => {
    let smtpTransport = 
    nodemailer.createTransport({
        service : 'gmail',
        secure : 'false',
        port : '25',
        auth: {
          user : cloudinaryDetails.email,
          pass : cloudinaryDetails.password
        },
        tls : {
          rejectUnauthorized : false
        }
    }) 
    let mailOptions = {
      to : foundPost.email,
      from : cloudinaryDetails.email,
      subject : 'Message from ' +req.user.name,
      text :'Hi ' + foundPost.name + ' , \n\n' +
      'You got a thank you message from your friend\n\n' +
      req.user.name +': I am so proud to be your friend :) . I also wish you love hope and everlasting joy and happiness. Thank you for being my friend.  \n\n' +
      'For wishing him/her from the wishbirds.' +
      'Thanks for supporting us.\n' +
      'www.wishbirds.com'
    };
    smtpTransport.sendMail(mailOptions, function(err){
      res.json({sent : 'Mail sent'})
    })
    res.json({id : foundPost.id})
  })
})


module.exports = router;
