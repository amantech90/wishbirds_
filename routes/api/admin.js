const express = require('express');
const router = express.Router();
const Admin = require('../../model/Admin')
const adminid = require('../../config/db').adminid
const User = require('../../model/User')
const Intern = require('../../model/Intern')
const passport = require('passport');



router.get('/' ,passport.authenticate('jwt' , {session : false}) , (req ,res) => {
User.findById(req.user.id).then(user => {
  if(user.isAdmin === 'true'){
    Admin.findById(adminid).then(all => {
      res.json(all)
    }).catch(err => {
      console.log(err)
    })
  }else{
    return res.status(400).json({unauthorize : 'You are not authorized for this action'})
  }
})

})

router.post('/interns' , passport.authenticate('jwt' , {session : false}) , (req , res) => {
 User.findById(req.user.id).then(user => {
   if(user.isAdmin === 'true'){
    const newIntern = new Intern({
      name : req.body.name,
      share : req.body.share,
      like : req.body.like,
      email : req.body.email,
      points : '0'
     
    })
     newIntern.save().then(intern => {
       res.json(intern)
     })
   }else{
    return res.status(400).json({unauthorize : 'You are not authorized for this action'})
  }
 })

})

router.post('/interns/postname/:id' , passport.authenticate('jwt' , {session : false}) , (req , res) => {
  User.findById(req.user.id).then(user => {
    if(user.isAdmin === 'true'){
     Intern.findById(req.params.id).then(intern => {
       const newPostDetails = {
         share : req.body.share,
         like : req.body.like,
         postname : req.body.postname
       }

       intern.posts.unshift(newPostDetails)
       intern.save().then(newintern => {
         res.json(intern)
       }).catch(err => res.json(err))
     })
    }else{
     return res.status(400).json({unauthorize : 'You are not authorized for this action'})
   }
  })
 
 })

 router.post('/intern/update/:id' ,passport.authenticate('jwt' , {session : false}), (req , res) => {
  User.findById(req.user.id).then(user => {
    if(user.isAdmin === 'true'){
      Intern.findById(req.params.id).then(intern => {
        intern.share = parseInt(req.body.updateShare) +parseInt(intern.share);
        intern.like = parseInt(req.body.updateLike) +parseInt(intern.like);
        intern.points = parseInt(intern.like)*5 +parseInt(intern.share)*3

        intern.save().then(newIntern=>res.json(newIntern))
      })
    }else{
     return res.status(400).json({unauthorize : 'You are not authorized for this action'})
   }
  })
 })

 router.get('/intern' , passport.authenticate('jwt' , {session : false}), (req , res) => {
  User.findById(req.user.id).then(user => {
    if(user.isAdmin === 'true' || user.isIntern === 'true'){
      Intern.find({}).sort({"points" : -1}).then(interns => {
          
  res.json(interns)

      })
      .catch(err => res.json(err))
    }else{
     return res.status(400).json({unauthorize : 'You are not authorized for this action'});
   }
  })
 })
 
module.exports = router;