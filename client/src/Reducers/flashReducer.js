import { FLASH_MESSAGE } from "../actions/types";

const intialState = {
  messages : null
}

export default function(state = intialState , action){

  switch(action.type){
    case FLASH_MESSAGE :
    return action.payload
  
    default :
    return state
  }

}