import { ADMINDATA, ADMINLOADING } from "../actions/types";


const initialState = {
   data : {},
   adminLoading : false
  }


  
  export default function(state = initialState , action){ 

    switch(action.type){
    
      case ADMINLOADING : 
      return{
        ...state,
        adminLoading : true
      }

      case ADMINDATA :
      return {
        ...state,
        data : action.payload,
        adminLoading : false
      }

      default :
      return state
    }

  }