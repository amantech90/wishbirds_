import {combineReducers} from 'redux';
import authReducer from './authReducer'
import errorReducer from './errorReducer';
import eventReducer from './eventReducer';
import flashReducer from './flashReducer'
import postReducer from './postReducer';
import adminReducer from './adminReducer'
import internsReducer from './internsReducer'
export default combineReducers({
  auth : authReducer,
  error : errorReducer,
  events : eventReducer,
  messages : flashReducer,
  posts : postReducer,
  admin : adminReducer,
  interns : internsReducer
})