import { SET_CURRENT_USER, USERLOADING, UPDATELOADING } from "../actions/types";
import isEmpty from "../validation/isEmpty";


const initialState = {
  isAuthenticated : false,
  user : {} ,
  userloading : false
  }

  export default function(state = initialState , action){
    switch (action.type) {
      case SET_CURRENT_USER : 
      return{
        ...state,
        isAuthenticated : !isEmpty(action.payload),
        user  : action.payload
      }
      case USERLOADING :
      return{
        ...state,
        userloading : true
      }
      case UPDATELOADING :
      return{
        ...state,
        userloading : false
      }
       default : 
       return state
    }
  }