import { GET_POST_DATA, POST_POST_DATA, POST_LOADING, THANK_LOADING, THANK_MESSAGE } from '../actions/types';

const initialState = {
  posts: [],
  Postloading: false,
  thankloading : false,
  postID : '',
  eventUpdate : false

};

export default function(state = initialState, action) {
  switch (action.type) {
    case POST_LOADING:
      return {
        ...state,
        Postloading: true
      };
      case THANK_LOADING:
      return {
        ...state,
        thankloading : true
      }

    case GET_POST_DATA:
      return Object.assign({} , state , {
        posts : action.payload,
        Postloading : false
      })
      
      case POST_POST_DATA:
      return Object.assign({} , state , {
        posts : [...state.posts ,action.payload ],
        Postloading : false
      })
    case THANK_MESSAGE :
    return {
      ...state ,
      postID : action.payload,
      thankloading : false

    }
    default:
      return state;
  }
}

