import { GET_EVENT_DATA, LOADING, POST_EVENT_DATA, DELETE_EVENT, GET_EVENT_DATA_ID, LOADING_DELETE_EVENT } from '../actions/types';

const initialState = {
  events: {},
  loading: false,
  deleteloading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case LOADING:
      return {
        ...state,
        loading: true
      };
      case LOADING_DELETE_EVENT:
      return {
        ...state,
        deleteloading: true
      };
    case GET_EVENT_DATA:
      return {
        ...state,
        events: action.payload,
        loading: false,
      };
      case POST_EVENT_DATA:
      return{
        ...state,
        events : action.payload,
        loading : false
      }
      case GET_EVENT_DATA_ID:
      return{
        ...state,
        events : action.payload,
        loading : false,
      }
      case DELETE_EVENT:
      return{
        ...state,
        events : Object.keys(state.events).remove,
        deleteloading : false
      }
    default:
      return state;
  }
}
