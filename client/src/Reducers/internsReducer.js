import { GET_INTERN ,LOADING_GET_INTERN,LOADING_POST_INTERN } from "../actions/types";

const initialState = {
  interns: [],
  loading: false,
};

export default function(state = initialState, action) {

switch(action.type){
  case LOADING_GET_INTERN : 
  return{
    ...state,
    loading : true
  }
  case LOADING_POST_INTERN : 
  return{
    ...state,
    loading : true
  }

  case GET_INTERN :
  return{
    ...state,
    interns : action.payload,
    loading : false
  }
  
  default:
  return state;
}

}