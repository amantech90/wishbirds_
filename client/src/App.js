import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import Layout from './component/layout';
import Landing from './component/landing/landing';
import Login from './component/auth/login/login';
import jwt_decode from 'jwt-decode';
import setAuthToken from '../src/utils/authtoken';
import { setCurrentUser, logoutUser } from './actions/registerAction';
import store from './store';
import PrivateRoute from './privateRoute';
import Post from './component/Post/post';
import asyncComponent from './hoc/asyncComponent';
import Error from './component/error/error';

const eventSync = asyncComponent(() => {
  return import('./component/dashboard/Event/event')
})

const adminSync = asyncComponent(() => {
  return import('./component/dashboard/adminDashoard/adminDashboard')
})

const forgotSync = asyncComponent(() => {
  return import('./component/auth/forgotpassword')
})

const profileSync = asyncComponent(() => {
  return import('./component/profile/profile')
})


const dashbosrdSync = asyncComponent(() => {
  return import('./component/dashboard/dashboard')
})

// check for token
if (localStorage.jwtToken) {
  //Set the auth token
  setAuthToken(localStorage.jwtToken);
  //decode token and get user info
  const decode = jwt_decode(localStorage.jwtToken);
  //Set user and isAuthenticated
  store.dispatch(setCurrentUser(decode));
  //Check for expired token
  const currentTime = Date.now() / 1000;
  if (decode.exp < currentTime) {
    //Logout user
    store.dispatch(logoutUser());

    //Redirect to login
    window.location.href = '/login';
  }
}
class App extends Component {
  render() {

    
        return (
      <div>
        <Layout>
 

  


          <Switch>
         
          <PrivateRoute path="/dashboard" exact component={dashbosrdSync} />
          <PrivateRoute path="/event" exact component={eventSync} />
          <PrivateRoute path="/profile" exact component={profileSync} />
          <Route path="/posts/:name" exact component={Post} />
          <PrivateRoute path="/admin" exact component={adminSync} />
          <Route path="/login" exact component={Login} />
          <Route path="/forgot/reset/:token" exact component={forgotSync} />
          <Route path="/" exact component={Landing} />
          <Route component={Error} />
         </Switch>
        </Layout>
 
      </div>
    );
  }
}
export default App;
