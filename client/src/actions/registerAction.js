import { GET_ERROR, SET_CURRENT_USER,USERLOADING, UPDATELOADING } from './types';
import axios from 'axios';
import setAuthToken from '../utils/authtoken';
import jwtdecode from 'jwt-decode';
import { sendFlashMessage } from './flashAction';


export const registerUser = (userData, history) => dispatch => {
  axios
    .post('/api/users/register', userData)
    .then(res =>
  {    //save to localstorage
    const { token } = res.data;

    //save to local storage
    localStorage.setItem('jwtToken', token);
    //set token to header
    setAuthToken(token);
    //decode
    const decode = jwtdecode(token);
    //set current user

    
    dispatch(setCurrentUser(decode));
    dispatch(sendFlashMessage({message : 'Thankyou '}))
  }
    
    )
    .catch(err =>
      dispatch({
        type: GET_ERROR,
        payload: err.response.data
      })
    );
};

export const loginUser = userData => dispatch => {
  axios
    .post('/api/users/login', userData)
    .then(res => {
      //save to localstorage
      const { token } = res.data;

      //save to local storage
      localStorage.setItem('jwtToken', token);
      //set token to header
      setAuthToken(token);
      //decode
      const decode = jwtdecode(token);
      //set current user

      dispatch(setCurrentUser(decode));
    })
    .catch(err =>
      dispatch({
        type: GET_ERROR,
        payload: err.response.data
      })
    );
};

export const setCurrentUser = decode => {
  return {
    type: SET_CURRENT_USER,
    payload: decode
  };
};

// logout user

export const logoutUser = () => dispatch => {
  //remove token from localstorage
  localStorage.removeItem('jwtToken');
  // Remove auth header for future requests
  setAuthToken(false);
  // Set current user to {} which will set isAuthenticated to false

  dispatch(setCurrentUser({}));

  window.location.href = '/login';
};

//profilePic update

export const updateprofilePic = (image) => dispatch => {
  dispatch(UserloadingData())
  axios.post('/api/users/profile/updateprofilepic' , image).then(res=>{
    dispatch(sendFlashMessage({message : 'Updated , after a logout and again sign in you can see your updated profile pic '}))
    dispatch(UpdateloadingData())
  }).catch(err => {
    dispatch({
      type: GET_ERROR,
      payload: err.response.data
    })
  })
  
}

export const updateChangePassword = (password) => dispatch => {
  dispatch(UserloadingData())
  axios.post('/api/users/profile/change_password' , password).then(res => {
    dispatch(sendFlashMessage({passwordMessage : 'Password updated! '}))
    dispatch(UpdateloadingData())
  }).catch(err => {
    dispatch({
      type: GET_ERROR,
      payload: err.response.data
    })
  })
}

export const forgotPassword = (email) => dispatch => {
  dispatch(UserloadingData())
  axios.post('/api/users/forgotpassword' , email).then(res => {
    dispatch(sendFlashMessage({message : `A mail has been sent to your entered email account`}))
    dispatch(UpdateloadingData())
  }).catch(err => {
    dispatch({
      type: GET_ERROR,
      payload: err.response.data
    })
  })
}

export const setPassword = (token , password) => dispatch => {
  dispatch(UserloadingData())
  axios.post(`/api/users${token}` , password).then(res => {
    dispatch(sendFlashMessage({message : 'Successfully updated !'}))
    dispatch(UpdateloadingData())
  }).catch(err => {
    dispatch({
      type: GET_ERROR,
      payload: err.response.data
    })
  })
}

export const UserloadingData = () => {
  return { type: USERLOADING };
};
export const UpdateloadingData = () => {
  return { type: UPDATELOADING };
};

