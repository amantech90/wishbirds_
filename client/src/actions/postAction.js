import axios from 'axios';
import { GET_POST_DATA, POST_POST_DATA, GET_ERROR , POST_LOADING, CLEAR_ERRORS,THANK_LOADING , THANK_MESSAGE} from './types';
import { eventDataById } from './eventAction';

export const getPostData = id => dispatch => {
  dispatch(postloadingData());

  axios
    .get(`/api${id}`)
    .then(res => {
      dispatch({
        type: GET_POST_DATA,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERROR,
        payload: err.response.data
      });
    });
};

export const postPostData = (userData, id) => dispatch => {
  dispatch(postloadingData());
  axios.post(`/api${id}`, userData).then(res => {
    dispatch({
      type: POST_POST_DATA,
      payload: res.data
    });
    dispatch(getPostData(`${id}`));
    dispatch(eventDataById(`/event${id}`));
  }).catch(err =>
    dispatch({
    type : GET_ERROR,
    payload : err.response.data
  }));
};

export const thank = (id) => dispatch=> {
  dispatch(postthankYou())
  axios.post(`/api/posts/thankyou/${id}`)
  .then(res => {
    dispatch({
      type : THANK_MESSAGE,
      payload : res.data
    })
  })
}

export const postloadingData = () => {
  return { type: POST_LOADING };
};
export const postthankYou = () => {
  return { type: THANK_LOADING };
};

// Clear errors
export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};

export const deletePost = (id , ownerId) => dispatch => {

axios.delete(`/api/posts/${id}`).then(res => {
  dispatch(getPostData(`/posts/${ownerId}`));
  dispatch(eventDataById(`/event/posts/${ownerId}`));
})


}


export const updateEvent = (eventId , option) => dispatch => {

  axios.post('/api/event/update' , option)
  dispatch(getPostData(`/posts/${eventId}`));
  dispatch(eventDataById(`/event/posts/${eventId}`));

}



