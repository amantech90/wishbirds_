import axios from 'axios';
import { GET_EVENT_DATA, LOADING, POST_EVENT_DATA, GET_ERROR,DELETE_EVENT , GET_EVENT_DATA_ID ,LOADING_DELETE_EVENT , LOADING_POST_EVENT } from './types';
import { sendFlashMessage } from './flashAction';
export const eventData = () => dispatch => {
  dispatch(loadingData());
  axios
    .get('/api/event')
    .then(res =>
   {   dispatch({
        type: GET_EVENT_DATA,
        payload: res.data
      })
    }
    )
    .catch(err =>
      dispatch({
        type: GET_ERROR,
        payload: err.response.data
      })
    );
};

export const createData = (eventData, history) => dispatch => {
  dispatch(loadingData());
 axios
    .post('/api/event', eventData)
    .then(res => {  
      dispatch({
        type : POST_EVENT_DATA,
        payload : res.data
      })
      history.push('/dashboard');
    dispatch(sendFlashMessage({message : 'You have created one'}))
    })
    .catch(err => 
    dispatch({
      type : GET_ERROR,
      payload : err.response.data
    }));
   
};

export const deleteEvent = (id) =>dispatch =>{
  dispatch(deleteloadingData());
  axios.post('/api/event/delete',id)
  .then(res=>{
    dispatch({
      type : DELETE_EVENT,
      payload : id
    })
    dispatch(eventData())
  })
}
export const eventDataById = (id) => dispatch => {
  dispatch(loadingData());
  axios
    .get(`/api${id}`)
    .then(res =>
   {   dispatch({
        type: GET_EVENT_DATA_ID,
        payload: res.data
      })
    }
    )
    .catch(err =>
      dispatch({
        type: GET_ERROR,
        payload: err.response.data
      })
    );
};
export const loadingData = () => {
  return { type: LOADING };
};

export const deleteloadingData = () => {
  return { type: LOADING_DELETE_EVENT };
};

export const postloadingData = () => {
  return { type: LOADING_POST_EVENT };
};