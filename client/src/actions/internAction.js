import axios from 'axios'
import { GET_INTERN, LOADING_GET_INTERN,LOADING_POST_INTERN } from './types';


export const getInterns = () => dispatch => { 
  dispatch(getloadingData());
  axios.get('/api/admin/intern').then(res => {
     dispatch({
       type :GET_INTERN,
       payload : res.data
     })
  })
}

export const postInterns = (data , id) => dispatch => {
  dispatch(postloadingData());
  axios.post(`api/admin/intern/update/${id}` , data).then(res => {
    dispatch(getInterns())
  })
}

export const getloadingData = () => {
  return { type: LOADING_GET_INTERN };
};

export const postloadingData = () => {
  return { type: LOADING_POST_INTERN };
};