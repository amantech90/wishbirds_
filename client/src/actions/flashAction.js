import { FLASH_MESSAGE } from './types';

export const sendFlashMessage = message => {
  return {
    type: FLASH_MESSAGE,
    payload: message
  };
};
