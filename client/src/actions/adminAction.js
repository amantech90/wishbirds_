import { ADMINDATA, ADMINLOADING } from "./types";
import Axios from 'axios'
export const adminData = () => dispatch => {
 dispatch(adminloadingData()) 
  Axios.get('/api/admin/').then(res => {
   dispatch({
     type : ADMINDATA,
     payload : res.data
   })
  }).catch(err => console.log(err))
}

export const adminloadingData = () => {
  return { type: ADMINLOADING };
};