import React, { Component } from 'react'
import classes from './post.css'
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { Image} from 'cloudinary-react';
import MediaQuery from 'react-responsive';
import { CircularProgress } from '@material-ui/core';
import {withRouter} from 'react-router-dom'
import {connect} from 'react-redux'
import isEmpty from '../../../validation/isEmpty';
import {deletePost , thank} from '../../../actions/postAction'
import {Link} from 'react-router-dom'
import Alert from '../../alert/alert';
import Fade from 'react-reveal';

 class Post extends Component {
  state = {
    anchorEl: null,
    loading2 : false,
    image : '',
    like : false,
    offline : false

  };

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
 
  };
   onClick = () => {
    if(navigator.onLine){this.props.deletePost(this.props.ey , this.props.ownerId)}
    else{this.setState({offline : !this.state.offline})} 
  }

  thankyou = (id) => {
   this.props.thank(id)
  }

  render() {
    const{user} = this.props.auth
    const {events} = this.props.events
    const{name , ey , image , wish } = this.props;

    let menu = null;
    const {postID , thankloading} = this.props.posts
    let thanksMessage = null;
    let btnCss = classes.Button
    if(isEmpty(user)){
     btnCss = classes.Disabled
    }
     if(postID.id === ey){
       thanksMessage = <p style={{fontSize:'10px' , color:'green' ,textAlign:'center' , padding:'5px'}}>Your message send successfully !.</p>
     }
     let btnText = 'Say Thank you'
     if(thankloading){
       btnText = <CircularProgress color="inherit"/>
     }
     if( !thankloading && isEmpty(user)){
       btnText = 'Login to Say Thank You'
     }

    if(!isEmpty(user) && !isEmpty(events)){
      menu =  user.id===events.owner._id ?<MenuItem onClick={this.onClick}>Delete</MenuItem> : <MenuItem>Delete ask the event creater</MenuItem>
    }else{
      menu = <MenuItem>Login To delete</MenuItem>
    }

    let alert = null;
    if(this.state.offline){
      alert = <Alert/>
    }
    const { anchorEl , loading2 } = this.state;
    let loading1 = null;
    if(loading2){
      loading1 = <CircularProgress/>
    }

    return (
      <div> 

  <Fade>
         <div>
          <div className={classes.Top}>
          <p className={classes.Name}>{name}</p>
          {/* <p className={classes.Date}>{date}</p> */}
        
          <div className={classes.Nav}
          aria-owns={anchorEl ? 'simple-menu' : null}
          aria-haspopup="true"
          onClick={this.handleClick}
          >
           <i className="fa fa-bars" />
          </div>
          </div>
            <div>
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              open={Boolean(anchorEl)}
              onClose={this.handleClose}
            >
              {menu}
            { !isEmpty(user) ? <MenuItem><Link to="/profile">My account</Link></MenuItem> : null}
              <MenuItem><a href={`https://res.cloudinary.com/wishbird/image/upload/${image}`} download>Download</a></MenuItem>
            </Menu>
            </div>

        <p style={{color:"#311053" , fontSize:'14px' , textAlign:'left' , marginLeft:'10px' ,marginTop:'-10px'}}>{wish}</p>
          </div>
  
          <div>
            
            <MediaQuery query="(max-width: 600px)">
            <div className={classes.Image500}>
            <Image cloudName="wishbird" publicId={image} width="500" height="400" background="grey" gravity="auto" crop="fill"> </Image>

            </div>
                
            </MediaQuery>
            <MediaQuery query="(max-width: 400px)">
            <Image cloudName="wishbird" publicId={image} width="400" height="400" background="grey" gravity="auto" crop="fill"> </Image>
            </MediaQuery>
              <div className={classes.ImagePc}>
              <Image cloudName="wishbird" publicId={image} width="600" height="400" background="grey" gravity="auto" crop="fill"> </Image>

              </div>

             {loading1}
           
            {alert}

          </div>
           <div className={classes.BottomNavBar}>
             <button  className={btnCss} type="button" onClick={(postid)=>this.thankyou(ey)}>{btnText}</button>
             {thanksMessage}
           </div>
          </Fade>
      </div>
    )
  }
}
const mapStatetoProps = state => ({
  auth : state.auth,
  events : state.events,
  posts : state.posts
})

export default connect(mapStatetoProps ,{ deletePost , thank})(withRouter(Post))
