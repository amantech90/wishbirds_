import React, { Component, Fragment } from 'react';
import classes from './post.css';
import NavBar from '../navbar/otherNavbar/Navbar';
import NavBar1 from '../navbar/navbar';
import { Grid } from '@material-ui/core';
import Footer from '../Footer/footer';
import {connect} from 'react-redux';
import Post from '../Post/post/post';
import { Image, Transformation } from 'cloudinary-react';
import CircularProgress from '@material-ui/core/CircularProgress';
import { getPostData , postPostData , updateEvent } from '../../actions/postAction';
import { withRouter } from 'react-router-dom';
import { eventDataById } from '../../actions/eventAction';
import isEmpty from '../../validation/isEmpty';
import Alert from '../alert/alert';
import {CopyToClipboard} from 'react-copy-to-clipboard';



class Posts extends Component {
  state = {
    wish : '',
    image : '',
    imagePreviewUrl : '',
    display : false,
    errors : {},
    offline : false,
    copied : false,
    name : '',
    email : '',
    loading: false
  }


  componentWillReceiveProps(nextProps) {
    if (nextProps) {
      this.setState({
        errors: nextProps.error
      });
      this.props.posts.Postloading = false
    }
  }
  onChange = e => {
    e.preventDefault();
    this.setState({
      [e.target.name]: e.target.value,
    
    });
  }

  displayFormOut = () => {
if(navigator.onLine){    
  this.setState({
    loading : true
  })
  
  let option = {
      saveoption : 'true'
    }
   this.props.updateEvent(this.props.event.events._id ,option )}
   else{
     this.setState({
       offline : !this.state.offline
     })
   }
  }

  onChange1 = e=>{
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
       image: file,
        imagePreviewUrl: reader.result
      });
    }


    reader.readAsDataURL(file)
  }
  
  onSubmit = e => {
    e.preventDefault();
if(navigator.onLine){    const posts =  
      { wish: this.state.wish,
        name: this.state.name,
        email:this.state.email,
      image : this.state.imagePreviewUrl}
      if(isEmpty(posts.image)){
        posts.image = 'https://res.cloudinary.com/wishbird/image/upload/v1520404003/xr5ddqr3xzzdmftzoqkk.jpg'
      }
    this.props.postPostData(posts, this.props.history.location.pathname);
    this.setState({
      wish:' ',
      name:" ",
      email : ' ',
      image:' '
    })
  }
    else{
      this.setState({
        offline : !this.state.offline
      })
    }
    }
 componentDidMount(){
if(navigator.onLine){   this.props.getPostData(this.props.history.location.pathname)
   this.props.eventDataById(`/event${this.props.history.location.pathname}`)

  }
 
 else{this.setState({
   offline : !this.state.offline
 })} 
  
  }
  render() {
    let alert = null;
    if(this.state.offline){
      alert = <Alert/>
    }
    const {posts , Postloading} = this.props.posts
    const {error} = this.props.error
    const {errors} = this.state
     let post = null
  const { events , loading} = this.props.event;
  const {user , isAuthenticated} = this.props.auth;
  let navbar = null;
  if(isAuthenticated){
  navbar = <NavBar/>
  }else{
    navbar = <NavBar1/>
  }
    document.title = `WishBirds || Birthday's made special`

  if(!Postloading && !isEmpty(posts) && isEmpty(error)){
    post = posts.map(post=> <Post name={post.name} key={post._id} ey={post._id} image={post.image} wish={post.wish} likes = {post.likes} ownerId={post.event} comments = {post.comments} date={post.date} />)
  }
  if(isEmpty(posts)){
    post = <h1 style={{textAlign : 'center'}}>No Posts Yet </h1>
  }
  let buttonText = 'Wish Now'
  if(Postloading){
    post = <CircularProgress style={{textAlign : 'center'}}/>
    buttonText = <CircularProgress/>

  }

  let btntext = 'Save'
  if(this.state.loading){
    btntext = <CircularProgress color = "inherit" />
  }

  let profileIntro = <CircularProgress/>
  if(!loading && !isEmpty(events)){
    profileIntro = (
      <Fragment>
      <Image style={{borderRadius : '50%'}} cloudName="wishbird" publicId={events.avatar}>
      <Transformation
        height="150"
        width="150"
        crop="fill"
        gravity="faces"
      />
    </Image>
      <div style={{ color: '#31105e', fontWeight: '500' }}>
       <h1> {events.name} </h1>
      </div>
      <div style={{ color: '#31105e ', padding: '5px' }}>
        {' '}
        <i className="fa fa-quote-left" /> {events.desc}
        <i className="fa fa-quote-right" />{' '}
      </div>
      </Fragment>
    )
  }else{
profileIntro = !loading?<h1>It's looks like events has been deleted or you have a broken link</h1>:<CircularProgress/>
  }
    return (
      <div className={classes.Post} >
        {navbar}
        <Grid container>
          <Grid item lg={12} md={12} xs={12} sm={12}>
            <div className={classes.Profile}>
            {profileIntro}
            </div>
          </Grid>
          <Grid item lg={12} md={12} xs={12} sm={12}>
            <div className={classes.Card}>
              {error?<h1>{error}</h1> : post}
            </div>
            {!isEmpty(events) ?  <div className={classes.CardForm} style = {events.saveoption==='true'?{display : 'none'} :{display : 'block'}}> 
           
           <h1  style={{textAlign:'center', padding:'10px'}}> Your wishes for {events.name}</h1>
             <form style={{textAlign:'center', padding:'10px'}} onSubmit={(e) => this.onSubmit(e)}>
             <input name="name" type="text" onChange={e => this.onChange(e)} placeholder="Name"/>
           {errors.name?<span style={{color : 'red' , fontSize:'14px'}}>{errors.name}</span>:null}

             <input name="email" type="email" onChange={e => this.onChange(e)} placeholder="Your Email Address"/>
           {errors.email?<span style={{color : 'red' , fontSize:'14px'}}>{errors.email}</span>:null}

               <input
             className = {classes.CustomFileInput}
             onChange={e => this.onChange1(e)}
             type="file"
             accept = "image/*"
           />
           <textarea onChange={e => this.onChange(e)} placeholder="Your wish" name="wish" />
           {errors.wish?<span style={{color : 'red' , fontSize:'14px'}}>{errors.wish}</span>:null}
           <span style={{color:'#a9a9a9' , fontSize:'10px'}}>WishBirds use auto cropping features, so some pictures might not fit. </span>
           <button>{buttonText}</button> 
                  
             </form>
             {events.owner._id === user.id ? <div> <button onClick={this.displayFormOut}>{btntext}</button><span style={{color:'#a9a9a9' , fontSize:'10px'}}>
             1. After getting all the wishes please click on the save button.<br/>
             2. Once you click on the save button the form will be submitted and no changes will be allowed. <br/>
             3. Forward the below link to your friend who's birthday we are celebrating. <br/>
            <p style={{textAlign : 'center'}}> <CopyToClipboard text={`https:/www.wishbirds.com/posts/${events._id}`}
          onCopy={() => this.setState({copied: true})}>
          <span>{`https:/www.wishbirds.com/posts/${events._id}`}</span>
        </CopyToClipboard></p>
        {this.state.copied ? <p style={{color : 'green' , textAlign : 'center'}}>copied to clipboard</p> : null}
             </span>  </div> : null}
    </div> : null }
            {alert}
          </Grid>
        </Grid> 
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  auth : state.auth,
  posts : state.posts,
  event : state.events,
  error : state.error,

})

export default connect(mapStateToProps, {eventDataById ,getPostData, postPostData , updateEvent})(withRouter(Posts))

