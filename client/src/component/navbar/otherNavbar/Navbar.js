import React, { Component , Fragment } from 'react';
import classes from './Navbar.css';
import { logoutUser } from '../../../actions/registerAction';
import { connect } from 'react-redux';
import Sidenavbar from '../SideNavbar/sidenavbar';
import {NavLink} from 'react-router-dom'
class OtherNavbar extends Component {
  state = {
    open: false
  };
  close = () => {
    this.setState({
      open: !this.state.open
    });
  };

  open = () => {
    this.setState({
      open: !this.state.open
    });
  };
  logoutUser = e => {
    e.preventDefault();
    this.props.logoutUser();
  };
  render() {

    return (
      <Fragment>
        <Sidenavbar show={this.state.open} clicked={this.close} logout={this.logoutUser} slided={this.close}/>
      <div className={classes.Nav}>
        <nav
          className={`navbar navbar-expand-lg static-top" ${classes.Navbar}`}
        >
          <NavLink className={`navbar-brand ${classes.NavbarBrand}`} to="/" exact>
            WishBirds
          </NavLink>
          <ul className="navbar-nav ml-auto">
            <li className={`nav-item ${classes.NavItem}`}>
              <NavLink className="nav-link" to="/dashboard" exact>
                Dashboard
              </NavLink>
            </li>
            <li className={`nav-item ${classes.NavItem}`}>
              <NavLink className="nav-link" to="/profile" exact>
                Profile
              </NavLink>
            </li>
            <li className={`nav-item ${classes.NavItem}`}>
              <a style={{color:'#fff'}}
                id="register"
                className="nav-link"
                onClick={this.logoutUser}
              >
                Logout
              </a>
            </li>
            <li className={`nav-item ${classes.NavItem1}`}>
              <a
                className="nav-link"
                onClick={this.open}
              >
                <i className="fa fa-bars" />
              </a>
            </li>
          </ul>
        </nav>
      </div>
      </Fragment>
    );
  }
}

export default connect(
  null,
  { logoutUser }
)(OtherNavbar);
