import React, { Component, Fragment } from 'react';
import { Drawer, Typography, List, Divider } from '@material-ui/core';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { NavLink, Link } from 'react-router-dom';
import classes from './navbar.css';
import Hamburger from '../image/hambar.png';
import { connect } from 'react-redux';
import { logoutUser } from '../../actions/registerAction';

class Navbar extends Component {
  state = {
    left: false,
    open : false
  };

  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open
    });
  };
  handleClickOpen = () => {
    this.setState({ open: true });
  };
  logoutUser = e => {
    e.preventDefault();
    this.props.logoutUser();
  };
  render() {
    const { isAuthenticated, user } = this.props.auth;
    const authLinks = (
      <nav>
        <Link to={isAuthenticated ? '/dashboard' : '/'}> WishBirds </Link>
        <a className={classes.Event} href="https://www.facebook.com/wish.wishbirds/" target="_blank" rel="noopener noreferrer"><i className="fa fa-facebook"></i></a>
        <a className={classes.Guide} href="https://plus.google.com/u/2/100702133012093303434" target="_blank" rel="noopener noreferrer"><i className="fa fa-google-plus"></i></a>
        <a className={classes.Guide} href="https://www.instagram.com/wishbirds/" target="_blank" rel="noopener noreferrer"> <i className="fa fa-instagram" ></i></a>
        <a onClick={this.toggleDrawer('left', true)}>
          <img src={Hamburger} height="50px" width="50px" alt="menu" />
        </a>
      </nav>
    );
    const guestLinks = (
      <nav>
        <Link to={isAuthenticated ? '/dashboard' : '/'}> WishBirds </Link>
        <a onClick={this.toggleDrawer('left', true)}>
          <img src={Hamburger} height="50px" width="50px" alt="menu" />
        </a>
      </nav>
    );
    const authList = (
      <div className={classes.List}>
        <List component="nav" className={classes.Lists}>
          <ListItem>
            <Typography variant="title" align="center">
              Wish<span className={classes.Birds}>Birds</span>
            </Typography>
          </ListItem>
          <Divider />

          <ListItem button>
            <NavLink to={isAuthenticated ? '/dashboard' : '/'} exact>
              <ListItemText>
                <p style={{ color: '#000066', fontWeight: 'bold' }}> Home </p>
              </ListItemText>
            </NavLink>
          </ListItem>
          <Divider />
          <ListItem button>
            <NavLink to="/profile" exact>
              <ListItemText>
                <p style={{ color: '#000066', fontWeight: 'bold' }}>
                  {' '}
                  Hi ! {user.name}{' '}
                </p>
              </ListItemText>
            </NavLink>
          </ListItem>
          <Divider />
          <ListItem button onClick={(e)=>this.props.logoutUser(e)}>
            <NavLink to="#">
              <ListItemText>
                <p style={{ color: '#000066', fontWeight: 'bold' }}>
                  {' '}
                  Log out{' '}
                </p>
              </ListItemText>
            </NavLink>
          </ListItem>
          <Divider />
        </List>
      </div>
    );
    const guestList = (
      <div className={classes.List}>
        <List component="nav" className={classes.Lists}>
          <ListItem>
            <Typography variant="title" align="center">
              Wish<span className={classes.Birds}>Birds</span>
            </Typography>
          </ListItem>
          <Divider />

          <ListItem button>
            <NavLink to={isAuthenticated ? '/dashboard' : '/'} exact>
              <ListItemText>
                <p style={{ color: '#000066', fontWeight: 'bold' }}> Home </p>
              </ListItemText>
            </NavLink>
          </ListItem>
          <Divider />
          <ListItem button>
            <NavLink to="/login" exact>
              <ListItemText>
                <p style={{ color: '#000066', fontWeight: 'bold' }}> Login </p>
              </ListItemText>
            </NavLink>
          </ListItem>
          <Divider />
          <ListItem button>
            <NavLink to="/login" exact>
              <ListItemText>
                <p style={{ color: '#000066', fontWeight: 'bold' }}>Register</p>
              </ListItemText>
            </NavLink>
          </ListItem>

          <Divider />
        </List>
      </div>
    );
    return (
      <Fragment>
        {isAuthenticated ? authLinks : guestLinks}
        <Drawer
          open={this.state.left}
          onClose={this.toggleDrawer('left', false)}
        >
          <div
            tabIndex={0}
            role="button"
            onClick={this.toggleDrawer('left', false)}
            onKeyDown={this.toggleDrawer('left', false)}
          >
            {isAuthenticated ? authList : guestList}
          </div>
        </Drawer>
      </Fragment>
    );
  }
}

const mapStatetoProps = state => ({
  auth: state.auth
});
export default connect(mapStatetoProps, { logoutUser })(Navbar);
