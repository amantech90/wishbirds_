import React, { Component, Fragment } from 'react';
import classes from './sidenavbar.css';
import Backdrop from '../../UI/Backdrop/Backdrop';
import { Image, Transformation } from 'cloudinary-react';
import classnames from 'classnames';
import { connect } from 'react-redux';

class SidenavBar extends Component {
  render() {
    const {user} = this.props.auth
    return (
      <Fragment>
        <Backdrop show={this.props.show} clicked={this.props.clicked} />
        <div
          className={
            this.props.show
              ? classnames(classes.Sidenav, classes.Open)
              : classnames(classes.Sidenav, classes.Close)
          }
        >
          <div className={classes.TopPart}> 
          <i onClick={this.props.clicked} className="fa fa-arrow-right"/>
          <Image
                style={{ borderRadius: '50%' }}
                cloudName="wishbird"
                publicId={user.avatar}
              >
                <Transformation
                  height="30"
                  width="30"
                  crop="fill"
                  gravity="faces"
                />
              </Image>
              <p>{user.name}</p>
           </div>
          <div className={classes.LowerPart}>
            <a  className="nav-link" href="/dashboard">
              Dashboard
            </a>
            <a  className="nav-link" href="/profile">
              Profile
            </a>
            <a className="nav-link" href="" onClick={this.props.logout}>Logout</a>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  null
)(SidenavBar);
