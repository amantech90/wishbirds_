import React from 'react'
import { Grid} from '@material-ui/core';
import classes from './footer.css'
const footer = () => {
  return (
    <footer className={classes.Footer}>

      <Grid container>
      
         <Grid item sm={12} xs={12} md={6} lg={6}>
         copyright &copy; {new Date().getFullYear()} Wishbirds.com
         </Grid>
         <Grid item sm={12} xs={12} md={6} lg={6}> <i className="fa fa-heart"></i>   made by Wishbirds</Grid>
       
      </Grid>

    </footer>
  )
}

export default footer