import React, { Component, Fragment } from 'react';
import classes from './register.css';
import rightPic from '../../image/right.png';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import CircularProgress from '@material-ui/core/CircularProgress';
import { registerUser } from '../../../actions/registerAction';
import isEmpty from '../../../validation/isEmpty';
import {Redirect} from 'react-router-dom'
import {sendFlashMessage} from '../../../actions/flashAction';
import Alert from '../../alert/alert';

class Register extends Component {
  state = {
    name: '',
    email: '',
    password: '',
    password2: '',
    avatar : '',
    errors: {},
    loading: false,
    offline : false,

  };

  componentWillReceiveProps(nextProps) {
    if (nextProps) {
      this.setState({
        errors: nextProps.errors,
        loading: false
      });
    }
  
  }

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSubmit = e => {
   if(navigator.onLine){let defaultAvatar = [
      "https://res.cloudinary.com/wishbird/image/upload/v1530511700/4_fqsgal.png",
      "https://res.cloudinary.com/wishbird/image/upload/v1530511700/3_r09nle.png",
      "https://res.cloudinary.com/wishbird/image/upload/v1530511700/1_bgugmu.png",
      "https://res.cloudinary.com/wishbird/image/upload/v1530511700/2_az33r7.png"
    ]
    let currentImage = defaultAvatar[Math.floor((Math.random()*defaultAvatar.length))]
    var avatar = currentImage
    e.preventDefault();
    const formData = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password,
      password2: this.state.password2,
      avatar : avatar
    };
    this.setState({
      loading: true
    });

    this.props.registerUser(formData, this.props.history);
  }else{
    this.setState({
      offline : !this.state.offline
    })
  }
    
  };


  render() {
    let alert = null;
    if(this.state.offline){
      alert = <Alert/>
    }
    const loading = this.state.loading;
    const { errors } = this.state;

    let loading1 =  <button> Register </button>;
    if (loading) {
      loading1 = (
        <CircularProgress
          className={classes.progress}
          thickness={3}
        />
      );
    }
    let errorClassName,
      errorClassEmail,
      errorClassPassword,
      errorClassPassword2 = {};

    if (!isEmpty(errors.name)) {
      errorClassName = classes.Error;
    }
    if (!isEmpty(errors.email)) {
      errorClassEmail = classes.Error;
    }
    if (!isEmpty(errors.password)) {
      errorClassPassword = classes.Error;
    }
    if (!isEmpty(errors.password2)) {
      errorClassPassword2 = classes.Error;
    }
    const {from} = this.props.location.state || {from : {pathname: '/dashboard'}}
    const {isAuthenticated} = this.props.auth
    if(isAuthenticated === true){
      return(
        <Redirect to = {from} />
      )
    }
    return (
      <Fragment>
        <div className={classes.Register}>

          <div className={classes.Loginbox}>
            <img
              src={rightPic}
              className={classes.UserAvatar}
              alt="user avatar"
            />

            <form className={classes.Form} onSubmit={e => this.onSubmit(e)}>
              <i className={'fa fa-user fa-2x ' + classes.cust} />
              <input
                className={errorClassName}
                type="text"
                placeholder="Name"
                name="name"
                onChange={e => this.onChange(e)}
              />
              <span className={classes.ErrorFeedback}>{errors.name}</span>
              <br />
              <i
                className={'fa fa-envelope  ' + classes.cust}
                style={{ fontSize: '25px' }}
              />
              <input
                className={errorClassEmail}
                type="email"
                placeholder="Email Address"
                name="email"
                onChange={e => this.onChange(e)}
              />
              <span className={classes.ErrorFeedback}>{errors.email}</span>
              <br />
              <i className={'fa fa-lock fa-2x ' + classes.cust} />
              <input
                className={errorClassPassword}
                type="password"
                placeholder="Password"
                name="password"
                onChange={e => this.onChange(e)}
              />
              <span className={classes.ErrorFeedback}>{errors.password}</span>
              <br />

              <i className={'fa fa-lock fa-2x ' + classes.cust} />
              <input
                className={errorClassPassword2}
                type="password"
                onChange={e => this.onChange(e)}
                placeholder="Confirm Password"
                name="password2"
              />
              <span className={classes.ErrorFeedback}>{errors.password2}</span>
            {loading1} 
            </form>

            {alert}
            <br />
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStatetoProps = state => ({
  auth: state.auth,
  errors: state.error,
  message : state.messages
});

export default withRouter(connect(
  mapStatetoProps,
  { registerUser, sendFlashMessage }
)(Register));
