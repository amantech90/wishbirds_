import React, { Component } from 'react'
import { Grid , CircularProgress } from '@material-ui/core';
import classes from './forgot.css'
import Navbar from '../navbar/navbar';
import Footer from '../Footer/footer';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {setPassword} from '../../actions/registerAction';
import isEmpty from '../../validation/isEmpty';
import Alert from '../alert/alert'
 class Forgot extends Component {

  state = {
    password : '',
    password1 : '',
    errors : {},
    offline : false
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps) {
      this.setState({
        errors: nextProps.errors
      });
      this.props.auth.userloading = false
    }
  }
  onChange = e => {
    this.setState({
      [e.target.name] : e.target.value
    })
  }

  changePassword = e => {
    e.preventDefault()
if(navigator.onLine){    const password = {
      password : this.state.password,
      password1 : this.state.password1
    }
    this.props.setPassword(this.props.history.location.pathname , password)
  }else{
    this.setState({
      offline : !this.state.offline
    })
  }
  }
  render() {
    let alert = null;
    if(this.state.offline){
      alert = <Alert/>
    }
    const { errors} = this.state;
    const{userloading} = this.props.auth;
    const {message} = this.props.message
    let btnText = 'Submit'
    let showInfo = null;

    if(!isEmpty(message)){
      showInfo = <div style={{backgroundColor:'green' , fontSize:'14px' , padding:'10px' , color:'#fff'}}>{message}</div>
    }


    if(userloading){
      btnText = <CircularProgress/>
    }
    return (
      <div className={classes.Forgot}>
        <Navbar/>
         <Grid>
             <Grid item xs={12} sm={12} lg={12} md={12}>

               <div className={classes.Form}>
               <br/>
             <br/>
             
               <p>One step away from changing your password</p>
               {errors.msg?<span style={{color : 'red' , fontSize:'14px'}}>{errors.msg}</span>:null}

               {showInfo}
               <form onSubmit={(e)=>this.changePassword(e)}>
            {errors.password?<span style={{color : 'red' , fontSize:'14px'}}>{errors.password}</span>:null}     <input type="password" onChange={(e)=>this.onChange(e)} name = "password" placeholder="New Password"/>
                 
                 <input type="password" onChange={(e)=>this.onChange(e)} name = "password1" placeholder="Confirm Password"/>
                 {errors.password1?<span style={{color : 'red' , fontSize:'14px'}}>{errors.password1}</span>:null}
               <button>{btnText} </button> 
              </form>
              {alert}
               </div>
             
             </Grid>

         </Grid>
         <br/>
        <Footer/>
      </div>
    )
  }
}
const mapStateToProps = state => ({
  message : state.messages,
  auth : state.auth,
  errors: state.error,
})
export default connect(mapStateToProps, {setPassword})(withRouter(Forgot))
