import React, { Component, Fragment } from 'react';
import classes from './login.css';
import rightPic from '../../image/right.png';
import shape from '../../image/shape-02-01.png';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { loginUser , forgotPassword } from '../../../actions/registerAction';
import CircularProgress from '@material-ui/core/CircularProgress';
import NavBar from '../../navbar/navbar'
import {Redirect} from 'react-router-dom'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import isEmpty from '../../../validation/isEmpty';
import Alert from '../../alert/alert'
import Register from '../register/register'
class Login extends Component {
  state = {
    email: '',
    password: '',
    display : false,
    errors: {},
    loading: false,
    open: false,
    forgotemail : '',
    internetConnection : false,
    registeropen : false
  };
  componentWillReceiveProps(nextProps) {



    if (nextProps) {
      this.setState({
        errors: nextProps.errors,
        loading: false
      });
      this.props.auth.userloading = false
    }
  }
  handleClickOpen = () => {
    this.setState({ open: true });
  };
  handleClickOpen1 = () => {
    this.setState({ registeropen: true });
  };
  handleClose = () => {
    this.setState({ open: false });
  };
  handleClose1 = () => {
    this.setState({ registeropen: false });
  };
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  onChange1 = e => {
    this.setState({
      forgotemail : e.target.value
    })
   
  }
  onSubmit = e => {
    e.preventDefault();
    if(navigator.onLine){
      const formData = {
        email: this.state.email,
        password: this.state.password
      };
      this.setState({
        loading: true
      });
      this.props.loginUser(formData);
    }else{
      this.setState({
        internetConnection : !this.state.internetConnection
      })
    }

  };
forgot = e => {
  e.preventDefault();
  if(navigator.onLine){
    const forgotEmail = {
      email1 : this.state.forgotemail
    }
    this.props.forgotPassword(forgotEmail)
  }else{
    this.setState({
      internetConnection : !this.state.internetConnection
    })
  }

}
  render() {
    let alert = null;
    if(this.state.internetConnection){
      alert = <Alert/>
    }
    const { errors, loading } = this.state;
    const{userloading} = this.props.auth;
    const {message} = this.props.message
    let btnText = 'Submit'
    let showInfo = null;
    if(!isEmpty(message)){
      showInfo = <Fragment> <span style={{backgroundColor:'green' , fontSize:'14px' , padding:'10px' , color:'#fff'}}>{message}</span> <br/> </Fragment>
    }


    if(userloading){
      btnText = <CircularProgress/>
    }
    let classErrorEmail,
      classErrorPassword = {};

             
  let loading1 = <button> Login </button> ;
    if (loading) {
      loading1 = (
        <CircularProgress
          className={classes.progress}
          thickness={3}
        />
      );
    }
    const {from} = this.props.location.state || {from : {pathname: '/dashboard'}}
    const {isAuthenticated} = this.props.auth
    if(isAuthenticated === true){
      return(
        <Redirect to = {from} />
      )
    }

    return (
        <div className={classes.Login}>
      <NavBar/>
        <div className={classes.Loginbox}>
          <img
            src={rightPic}
            className={classes.UserAvatar}
            alt="user avatar"
          />
          <form className={classes.Form} onSubmit={e => this.onSubmit(e)}>
            <i className={'fa fa-user fa-2x ' + classes.cust} />
            <input
              className={classErrorEmail}
              type="email"
              placeholder="Email Address"
              name="email"
              onChange={e => this.onChange(e)}
            />

            <span className={classes.ErrorFeedback}>{errors.email}</span>
            <br />
            <i className={'fa fa-lock fa-2x ' + classes.cust} />
            <input
              className={classErrorPassword}
              type="password"
              placeholder="Password"
              name="password"
              onChange={e => this.onChange(e)}
            />

            <br />
            <span className={classes.ErrorFeedback}>{errors.password}</span>
             {loading1} 
          </form>

          <div className={classes.Signup}>
            New to WishBirds ?{' '}
            <span style={{ marginLeft: '5px' }}>
              <a  onClick={this.handleClickOpen1}> Join Us</a>
            </span>
          </div>
          <br />
         {alert}
          <Button onClick={this.handleClickOpen}>Forgot Password</Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Forgot Password ?</DialogTitle>
          <DialogContent>
            <DialogContentText>
            {showInfo}
              For getting your password , Enter your email address.
              {errors.email1?<span style={{color : 'red' , fontSize:'14px'}}>{errors.email1}</span>:null}
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              id="name"
              label="Email Address"
              type="email"
              onChange = {(e) =>this.onChange1(e)}
              fullWidth
              required
            />
          </DialogContent>
          <DialogActions>
            <Button  color="primary">
              Cancel
            </Button>
            <Button onClick={(e) =>this.forgot(e)} color="primary">{btnText} </Button>

          </DialogActions>
        </Dialog>
       {/* Register dialogue */}
        <Dialog
          open={this.state.registeropen}
          onClose={this.handleClose1}
          aria-labelledby="form-dialog-title"
        >
       
          <DialogContent>
             <Register/>
          </DialogContent>
        </Dialog>
          <img
            className={classes.Hidden}
            src={shape}
            width="100%"
            height="100%"
            alt="wishbirds.com"
          />
        </div>
        </div>

    );
  }
}
const mapStatetoProps = state => ({
  auth: state.auth,
  errors: state.error,
  message: state.messages
});

export default withRouter(connect(mapStatetoProps, { loginUser , forgotPassword })(Login));
