import React from 'react'
import classes from './error.css'
import image from '../../component/image/WishBirdError404.png'
import NavBar from '../navbar/navbar';
import {Link} from 'react-router-dom'
const Error = (props) => {
  return (
    <div className={classes.Error}>
      <NavBar/>
      <div className={classes.ErrorMsg}>
           <img className={classes.Img} src={image} width="250px" height="250px" alt="404"/>
           <p>Oh! snap , You have entered wrong url</p>
           <Link to="/dashboard" >Go To Home</Link>
      </div>
    </div>
  )
}

export default Error