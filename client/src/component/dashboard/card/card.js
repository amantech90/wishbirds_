import React, { Component } from 'react';
import classes from './card.css';
import { deleteEvent } from '../../../actions/eventAction';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Image, Transformation } from 'cloudinary-react';
import {
  FacebookShareButton,
  TwitterShareButton,
  TelegramShareButton,
  WhatsappShareButton,
  FacebookIcon,
  TwitterIcon,
  TelegramIcon,
  WhatsappIcon,

} from 'react-share';
import IconButton from '@material-ui/core/IconButton';
import ShareIcon from '@material-ui/icons/Share'
import { Link } from 'react-router-dom';
import Alert from '../../alert/alert';
import 'moment-timezone';
import { CircularProgress } from '@material-ui/core';

class Card extends Component {
  state = {
    loading: false,
    offline: false,


  };

  onClick = id => {
    if (navigator.onLine) {
      this.props.deleteEvent(id, this.props.history);
    } else {
      this.setState({ offline: !this.state.offline });
    }
  };


  render() {
    const { deleteloading} = this.props.events
    let deleteIcon = null
    if(!deleteloading){
      deleteIcon = <i className="fa fa-trash"></i>
    }else{
      deleteIcon = <CircularProgress/>
    }
    let alert = null;
    if (this.state.offline) {
      alert = <Alert />;
    }
  
    return (
      <div className={classes.Container}>
        <div className={classes.Card}>
        <div className={classes.Top}>
         <p>{this.props.name}</p>
         {/* <p className={classes.Date}>{this.props.date}</p> */}
         <div className={classes.Menu} onClick = {(id)=>this.onClick(this.props.id)}>
            {deleteIcon}
         </div>
        </div>
          <div className={classes.Avatar}>
            <Image cloudName="wishbird" publicId={this.props.image}>
              <Transformation
                height="250"
                width="350"
                crop="fill"
                gravity="faces"
              />
            </Image>
          </div>
          <div className= {classes.Desc}>
        {this.props.desc}
          </div>
          <div className={classes.Share}>
          <IconButton className={classes.ShareButton} aria-label="Share">
              <ShareIcon />
            </IconButton>
          <ul className={classes.SubButton}>
          <li>                  <FacebookShareButton
                    className={classes.SmallIcon}
                    url={`https://www.wishbirds.com/posts/${this.props.id}`}
                  >
                    <FacebookIcon size={30} round={true} />
                  </FacebookShareButton></li>
          <li><WhatsappShareButton
                    url={`https://www.wishbirds.com/posts/${this.props.id}`}
                    className={classes.SmallIcon}
                  >
                    <WhatsappIcon size={30} round={true} />
                  </WhatsappShareButton></li>
          <li>                  <TwitterShareButton
                    url={`https://www.wishbirds.com/posts/${this.props.id}`}
                    className={classes.SmallIcon}
                  >
                    <TwitterIcon size={30} round={true} />
                  </TwitterShareButton></li>
                  <li>
                  {' '}
                  <TelegramShareButton
                    url={`https://www.wishbirds.com/posts/${this.props.id}`}
                    className={classes.SmallIcon}
                  >
                    <TelegramIcon size={30} round={true} />
                  </TelegramShareButton>{' '}
                </li>
        </ul>
        <span  style={{fontSize:'10px' , color : '#a7a7a7'}}>Share this link to your friends so that thay can post a wishes</span>
          </div>
         {alert}
          <Link to={`posts/${this.props.id}/`}>
              <p style={{fontSize:'20px' , padding : '10px'}}>Go to your wishes</p>
            </Link>
        </div>

      </div>
    );
  }
}

const statetoprops = state => ({
  events : state.events
})

export default connect(
  statetoprops,
  { deleteEvent }
)(withRouter(Card));
