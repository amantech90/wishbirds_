import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import classes from './dashboard.css';
import isEmpty from '../../validation/isEmpty';
import { withRouter } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { CircularProgress } from '@material-ui/core';
import { eventData, createData } from '../../actions/eventAction';
import { registerUser } from '../../actions/registerAction';
import OtherNavbar from '../navbar/otherNavbar/Navbar'
import Card from './card/card';
import Snackbar from '../UI/snackbar';
import { sendFlashMessage } from '../../actions/flashAction';
import Guidetour from '../GuideTour/guidetour';
import Footer from '../Footer/footer'
import Alert from '../alert/alert';
import Admin from '../dashboard/adminDashoard/adminDashboard'
// import Interns from '../dashboard/internDashboard/internDashboard'

class Dashboard extends Component {
state = {
  offline : false
}
  componentDidMount() {
    if(navigator.onLine){
      this.props.eventData();
    }else{
      this.setState({
        offline : !this.state.offline
      })
    }
  
  }
  render() {
    let alert = null;
    if(this.state.offline){
      alert = <Alert/>
    }
    const { events, loading } = this.props.events;
    const { user } = this.props.auth;
  document.title = `Hii ${user.name} || WishBirds-Birthdays made special`
    const { message } = this.props.message;
    let eventData;
    let loading1 = '';
    if (loading) {
      loading1 = <CircularProgress />;
    }

    if (!isEmpty(events)) {
      eventData = (
        <Fragment>
          <Card
            name={events.name}
            image={events.avatar}
            desc={events.desc}
            owner={events.owner.name}
            id={events._id}
            userId={user.id}
            date = {events.date}
          />
        </Fragment>
      );
    } else {
      eventData = (
        <Fragment>
        <div className={classes.Dashboard}>
          <div className={classes.Greeting}>
            <h1>Hi ! {user.name} </h1>
            <p>A new journey awaits you ! Welcome on aboard. </p>
            
          <Link to="/event">
            <button className={classes.Button}>Start your journey</button>{' '}
          </Link>
          </div>

        </div> 
       </Fragment>
      );
    }
    return (
      <Fragment>
      {user.isAdmin === 'true'?<Fragment><OtherNavbar/><Admin/></Fragment>: (<div className={classes.Dashboard1}>
        <OtherNavbar />
        {loading1}
        {eventData}
        {message ? (
          <Snackbar show={true} color="green">
            {message}
          </Snackbar>
        ) : null}
      </div>)}
      {alert}
      {}
      {user.isAdmin?null: user.isIntern === 'true'?null :<Guidetour/>} 
     <Footer/>
      </Fragment>
    );
  }
}
const mapStatetoProps = state => ({
  events: state.events,
  loading: state.loading,
  auth: state.auth,
  errors: state.error,
  message: state.messages
});
export default connect(
  mapStatetoProps,
  {
    eventData,
    registerUser,
    createData,
    sendFlashMessage
  }
)(withRouter(Dashboard));
