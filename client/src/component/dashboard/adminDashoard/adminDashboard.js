import React, { Component } from 'react';
import classes from './admin.css';
import { Grid, CircularProgress } from '@material-ui/core';
import { connect } from 'react-redux';
import { adminData } from '../../../actions/adminAction';
import isEmpty from '../../../validation/isEmpty';
import AdminForm from '../adminDashoard/adminForm';
import {getInterns} from '../../../actions/internAction'
class AdminDashboard extends Component {
  componentDidMount() {
    this.props.adminData();
    this.props.getInterns()
  }

  render() {
    const { data, adminLoading } = this.props.admin;
    const {interns,loading} = this.props.interns
    let users = null;
    let interns1 = null;
    if(!isEmpty(interns) && !loading){
      interns1 = interns.map(intern => (<AdminForm key={intern._id} name={intern.name} email={intern.email} share={intern.share} like={intern.like} id={intern._id} loading={loading}/>))
    }
    if(loading){
      interns1 = <CircularProgress/>
    }
    if (!isEmpty(data) && !adminLoading) {
      users = data.users.length+1;
    } else {
      users = <CircularProgress />;
    }
    let events = null;
    if (!isEmpty(data) && !adminLoading) {
      events = data.events.length ;
    } else {
      events = <CircularProgress />;
    }
    let posts = null;
    if (!isEmpty(data) && !adminLoading) {
      posts = data.posts.length;
    } else {
      posts = <CircularProgress />;
    }
    let comments = null;
    if (!isEmpty(data) && !adminLoading) {
      comments = data.comments.length ;
    } else {
      comments = <CircularProgress />;
    }
    let likes = null;
    if (!isEmpty(data) && !adminLoading) {
      likes = data.likes.length ;
    } else {
      likes = <CircularProgress />;
    }
    return (
      <div className={classes.Admin}>
        <h1>Welcome Aboard ! WishBirds</h1>
        <p>Here is your Dashboard</p>
        <br />
        <br />
        <Grid container spacing={16}>
          <Grid item xs={12} sm={12} md={6} lg={6}>
            <i className="fa fa-users" />
            <p>Total Users</p>
            {users}
          </Grid>
          <Grid item xs={12} sm={12} md={6} lg={6}>
            <i className="fa fa-wpforms" />

            <p>Total Events</p>
            {events}
          </Grid>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <i className="fa fa-pencil" />
            <p>Total Posts</p>
            {posts}
            <br />

            <i className="fa fa-birthday-cake" />
            <br />
            <p>Total Likes</p>
            {likes}

            <br />

            <i className="fa fa-comments" />

            <p>Total comments</p>
            {comments}

            <br />
          </Grid>
      
   

        </Grid>
        {interns1}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  admin: state.admin,
  interns : state.interns
});

export default connect(
  mapStateToProps,
  { adminData , getInterns }
)(AdminDashboard);
