import React, { Component } from 'react'
import classes from './adminForm.css'
import {Grid, CircularProgress} from '@material-ui/core'
import {connect} from 'react-redux';
import {postInterns} from '../../../actions/internAction'
import isEmpty from '../../../validation/isEmpty';
class adminForm extends Component {

state = {
  updateLike : '',
  updateShare : ''
}
onChange = (e) => {
  e.preventDefault()
    this.setState({
      [e.target.name]: e.target.value
    });
}
  onClick = (id1) => {
    if(isEmpty(this.state.updateLike && this.state.updateShare)){
      alert('please fill out this')
    }else{
      const data = {
        updateLike : this.state.updateLike,
        updateShare : this.state.updateShare
      }
  
      this.props.postInterns(data , id1)
    }

  }
  render() {
    const {name , email , like ,share , id ,loading} = this.props
    let btnText = 'Update'
    if(loading){
      btnText = <CircularProgress/>
    }
    return (
      <div className={classes.Form}>
        <Grid container>
          <Grid container item sm={12} xs={12} md={2} lg={2}>
        

          {name}
          </Grid>
          <Grid item sm={12} xs={12} md={3} lg={3}>
         

          {email}
          </Grid>
          <Grid item sm={12} xs={12} md={2} lg={1}>
          <label>Share</label>
          <input type="text"  onChange = {e =>this.onChange(e)} required name="updateShare" placeholder={share}/>
          </Grid>
          <Grid item sm={12} xs={12} md={1} lg={1}>
            .
          </Grid>
          <Grid item sm={12} xs={12} md={2} lg={2}>
          <label>Like</label>
          <input type="text" onChange = {(e) =>this.onChange(e)} required name="updateLike" placeholder={like}/>
          </Grid>
          <Grid item sm={12} xs={12} md={1} lg={1}>
           .
</Grid>
          <Grid item sm={12} xs={12} md={2} lg={2}>
           <button onClick = {(id1) =>this.onClick(id)}>{btnText}</button>
          </Grid>
        </Grid>
      </div>
    )
  }
}
export default connect(null, {postInterns})(adminForm)
