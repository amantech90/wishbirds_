import React, { Component } from 'react';
import Navbar from '../../navbar/otherNavbar/Navbar';
import classes from './event.css';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { createData } from '../../../actions/eventAction';
import { CircularProgress } from '@material-ui/core';
import avatar from '../../image/right.png'
import Alert from '../../alert/alert';

class Event extends Component {
  state = {
    name: '',
    image: '',
    desc: '',
    errors: {},
    imagePreviewUrl : '',
    offline : false
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps) {
      this.setState({
        errors: nextProps.errors
      });
      this.props.event.loading = false
    }
  }
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value,
    
    });
  };

  onChange1 = e=>{
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
       image: file,
        imagePreviewUrl: reader.result
      });
    }

    reader.readAsDataURL(file)
  }

  onSubmit = e => {
    e.preventDefault();
if(navigator.onLine){    const formData = {
      name: this.state.name,
      desc: this.state.desc,
      image : this.state.imagePreviewUrl
    };
    const event = formData;
    this.props.createData(event, this.props.history);}
    else{
      this.setState({
        offline : !this.state.offline
      })
    }
  };

  render() {
    let alert = null;
    if(this.state.offline){
      alert = <Alert/>
    }
    const errors = this.state.errors
    const { loading } = this.props.event;
    const { user } = this.props.auth;
    let loading1 =  <button onClick={e => this.onSubmit(e)}>Submit </button>;
    if (loading) {
      loading1 = <CircularProgress color="inherit"/>;
    }
    return (
      <div className={classes.Event}>
        <Navbar />
        <div className={classes.Form}>
        <img src={avatar} alt={user.name} width="100px" height="100px"/>
          <p>Hii {user.name} create an event</p>
          {errors.msg?<span style={{color : 'red' , fontSize:'14px'}}>{errors.msg}</span>:null}

          <form encType="multipart/form-data">
            <input placeholder="Name" onChange={e => this.onChange(e)} type="text" name="name" />
              {errors.name?<span style={{color : 'red' , fontSize:'14px'}}>{errors.name}</span>:null}
            <input
              className = {classes.CustomFileInput}
              onChange={e => this.onChange1(e)}
              type="file"
              accept = "image/*"
            />
              {errors.image?<span style={{color : 'red' , fontSize:'14px'}}>{errors.image}</span>:null}

            <textarea onChange={e => this.onChange(e)} placeholder={`about ${this.state.name}`} name="desc" />
            {errors.desc?<span style={{color : 'red' , fontSize:'14px'}}>{errors.desc}</span>:null}

       {loading1}
          </form>
        </div>
        {alert}
      </div>
    );
  }
}
const mapStateToProps = state => ({
  event: state.events,
  loading: state.loading,
  auth: state.auth,
  errors : state.error
});
export default connect(
  mapStateToProps,
  { createData }
)(withRouter(Event));
