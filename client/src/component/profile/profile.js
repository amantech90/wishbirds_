import React, { Component } from 'react';
import classes from './profile.css';
import Navbar from '../navbar/otherNavbar/Navbar';
import { connect } from 'react-redux';
import { Image, Transformation } from 'cloudinary-react';
import { eventData } from '../../actions/eventAction';
import isEmpty from '../../validation/isEmpty';
import Footer from '../Footer/footer';
import { Grid, CircularProgress } from '@material-ui/core';
import {updateprofilePic , updateChangePassword} from '../../actions/registerAction'
import Alert from '../alert/alert';

class Profile extends Component {
  state = {
    display: 'none',
    image : '',
    imagePreviewUrl : '',
    currentPassword : '',
    newPassword : '',
    newPassword1 : '',
    errors : {},
    offline : false
  };

  componentDidMount() {
  if(navigator.onLine) { this.props.eventData();}
  else{
    this.setState({
      offline : !this.state.offline
    })
  }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps) {
      this.setState({
        errors: nextProps.errors
      });
      this.props.auth.userloading = false
    }
  }

  onChange1 = e=>{
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
       image: file,
        imagePreviewUrl: reader.result
      });
    }

    reader.readAsDataURL(file)
  }
updatePic = (e) => {
  e.preventDefault();
if(navigator.onLine){  const image = {
    image : this.state.imagePreviewUrl
  }

 this.props.updateprofilePic(image)
 this.setState({
   userLoading : true
 })}else{
   this.setState({
     offline : !this.state.offline
   })
 }

}
onChange = (e) => {
  e.preventDefault()
  this.setState({
    [e.target.name] : e.target.value
  })
}
changePassword = (e) => {
  e.preventDefault();
if(navigator.onLine){  const password = {
    password : this.state.currentPassword,
    password1 : this.state.newPassword,
    password2 : this.state.newPassword1,
  }
  this.props.updateChangePassword(password)}
  else{
    this.setState({
      offline : !this.state.offline
    })
  }
}


  render() {
    let alert = null;
    if(this.state.offline){
      alert = <Alert/>
    }
    const {message , passwordMessage} = this.props.message;
    const errors = this.state.errors
    const { user , userloading } = this.props.auth;
    const { events, loading } = this.props.event;
    let eventData = null;
    let showMessage = null   
    let btnText = 'Submit' 
    if(userloading){
      btnText = <CircularProgress/>
    }
    if(!isEmpty(message)){
      showMessage =<p style={{backgroundColor:'green' , fontSize:'14px' , padding:'10px' , color:'#fff'}}>{message}</p>

    }
    let showPasswordMessage = null
    if(!isEmpty(passwordMessage)){
      showPasswordMessage =<p style={{backgroundColor:'green' , fontSize:'14px' , padding:'10px' , color:'#fff'}}>{passwordMessage}</p>

    }

    if(loading){
      eventData = <CircularProgress/>
    }
    if (!isEmpty(events) && !loading && navigator.onLine) {
      eventData = 
        <div>
          <p className={classes.Info}>
            On going Event :{' '}
            <span className={classes.Details}>{events.name}</span>
          </p>
          <p className={classes.Info}>
            Total Post :{' '}
            <span className={classes.Details}>{events.posts.length}</span>
          </p>
        </div>
    }

    if(!loading && isEmpty(eventData)){
      eventData = <h1>No event you have created</h1>
    }
   if(!navigator.onLine){
     eventData = "No internet connection"
   }
    return (
      <div className={classes.Profile}>
        <Navbar />
        <Grid>
   
          <Grid item xs={12} sm={12} lg={12} md={12}>
            <div className={classes.ProfileIntro}>
            <h1>User Info</h1> <hr/> <br/>
              <Image
                style={{ borderRadius: '50%' }}
                cloudName="wishbird"
                publicId={user.avatar}
              >
                <Transformation
                  height="150"
                  width="150"
                  crop="fill"
                  gravity="faces"
                />
              </Image>
              <p className={classes.Info}>
                Name : <span className={classes.Details}>{user.name}</span>
              </p>
              <p className={classes.Info}>
                Email : <span className={classes.Details}>{user.email}</span>
              </p>
              </div>
              </Grid>
              <Grid item xs={12} sm={12} lg={12} md={12}>
              <div className={classes.ProfileIntro}>
              <h1>Event Info</h1> <hr/> <br/>
              {eventData}
              </div>
             </Grid>
             <Grid item xs={12} sm={12} lg={12} md={12}>
              <div className={classes.ProfileIntro}>
              {showMessage}
              <h1>Update Profile Pic ?</h1> <hr/> <br/>

               <form onSubmit = {(e) => this.updatePic(e)}>
                 <input className={classes.CustomFileInput} onChange={(e) => this.onChange1(e)} type="file" name = "image"/>
                 {errors.image?<span style={{color : 'red' , fontSize:'14px'}}>{errors.image}</span>:null}
                <button>{btnText}</button>
              </form>
              </div>
             </Grid>
             <Grid item xs={12} sm={12} lg={12} md={12}>
              <div className={classes.ProfileIntro}>
              {showPasswordMessage}
              <h1>Update Password ?</h1> <hr/> <br/>
               <form onSubmit={(e)=>this.changePassword(e)}>
                 <input type="password" onChange={(e)=>this.onChange(e)} name = "currentPassword" placeholder="Current Password"/>
                 {errors.password?<span style={{color : 'red' , fontSize:'14px'}}>{errors.password}</span>:null}
                 <input type="password" onChange={(e)=>this.onChange(e)} name = "newPassword" placeholder="New Password"/>
                 {errors.password1?<span style={{color : 'red' , fontSize:'14px'}}>{errors.password1}</span>:null}
                 <input type="password" onChange={(e)=>this.onChange(e)} name = "newPassword1" placeholder="Confirm Password"/>
                 {errors.password2?<span style={{color : 'red' , fontSize:'14px'}}>{errors.password2}</span>:null}
                <button>{btnText}</button>
              </form>
              </div>
             </Grid>
             <Grid item xs={12} sm={12} lg={12} md={12}>
              <div className={classes.ProfileIntro}>
              <h1>WishBirds Message</h1> <hr/> <br/>
              <p className={classes.Message}>Thanks for using WishBirds. It's been a great opportunity to provide you this service.We are working hard to provide you with a better experience. </p>
             <p className={classes.Info}><i className="fa fa-heart"></i></p>
              </div>
             </Grid>
           {alert}
        </Grid>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  event: state.events,
  message : state.messages,
  errors : state.error
});

export default connect(
  mapStateToProps,
  { eventData , updateprofilePic , updateChangePassword }
)(Profile);
