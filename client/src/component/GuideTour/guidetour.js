import React, { Component } from 'react';
import { Grid, Paper } from '@material-ui/core';
import classes from './guidetour.css';
class Guidetour extends Component {


  render() {
    const style= {
      padding : '10px',
      height : '150px',
      background : 'none'
    }
    return (

        <div className={classes.Guide}>
          <Grid container spacing={16}>
            <Grid item xs={12} sm={6} md={3} lg={3}>
              <Paper  style={style}>
                <i className="fa fa-birthday-cake" />
                <p>Create an Event</p>
              </Paper>
            </Grid>
            <Grid item xs={12} sm={6} md={3}lg={3}>
              <Paper style={style}>
                <i className="fa fa-wpforms" />
                <p>Fill up the forms and generate the link</p>
              </Paper>
            </Grid>
            <Grid item xs={12} sm={6} md={3} lg={3}>
              <Paper style={style}>
                <i className="fa fa-share" />
               <p> Forward the link through social media </p>
              </Paper>
            </Grid>
            <Grid item xs={12} sm={6} md={3} lg={3}>
              <Paper style={style}>
                <i className="fa fa-heart" />
               <p> Voila. Your friends have loads of wishes on their birthday </p>
              </Paper>
            </Grid>
          </Grid>
        </div>
    );
  }
}

export default Guidetour;
