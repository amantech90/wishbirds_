import React, { Component } from 'react';
import classes from './snackbar.css'

 class SnackBar extends Component {

  state = {
    show: this.props.show,
    color : this.props.color
  }

  componentDidMount(){
    setTimeout(() => {
      this.setState({
        show : !this.state.show
      })
    }, 2000);
  }
  
  render(){
    const {show} = this.state
    let classes1 = classes.Hidden
    if(show){
      classes1 = classes.Snackbar
    }
    return(
      <div className={classes1}>
        <div style={{backgroundColor: this.state.color , width:'100%' , padding : '10px' , borderRadius : '10px'}}>
        {this.state.color ==='green' ? <i className="fa fa-check" style={{position:'absolute' , left:'20px'}}></i> :<i className="fa fa-times" style={{position:'absolute' , left:'20px'}}></i> }
        
        {this.props.children}
      </div>
      </div>
    );
  }
   
  }
  SnackBar.defaultProps = {
    show: "false",
    color : 'green'
  };
  export default SnackBar




