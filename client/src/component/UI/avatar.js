import React, { Fragment } from 'react'
import classes from './avatar.css'
const avatar = (props) => {
  return (
   <div className={classes.Center}> 
    <p className={classes.Avatar}>
      {props.children} 
    </p>
    </div>
  )
}

export default avatar

