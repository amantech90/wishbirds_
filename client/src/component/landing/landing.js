import React, { Fragment } from 'react';
import classes from './landing.css';
import shape from '../image/shape-02-01.png';
import { Link } from 'react-router-dom';
import NavBar from '../navbar/navbar';

const Landing = props => {
  return (
    <Fragment>
      <div className={classes.Landing}>
        <NavBar />
        <header>
          <h1>Birthday's made special</h1>
          <p>
            Someone's birthday ? Leave the wishes to us. Register now and let
            the magic happens.{' '}
          </p>

          <hr />
          <Link to="/login">
            <button className={classes.Btnfill}>Register now</button>
          </Link>
          {/* <Link to="wishbirds_card">
          <button className={classes.Btnfill}>Checkout our cards</button>
          </Link> */}
          <div className={classes.Social}>
            <ul>
              <li>
                <a href="https://www.facebook.com/wish.wishbirds/" target="_blank" rel="noopener noreferrer">
                  <i className="fa fa-facebook" aria-hidden="true" /> 
                </a>
              </li>
              <li>
                <a href="https://plus.google.com/u/2/100702133012093303434" target="_blank" rel="noopener noreferrer">
                  <i className="fa fa-google-plus" aria-hidden="true" />
                </a>
              </li>
              <li>
                <a href="https://www.instagram.com/wishbirds/" target="_blank" rel="noopener noreferrer">
                  <i className="fa fa-instagram" aria-hidden="true"  />
                </a>
              </li>
            </ul>
          </div>
          <br />
          <br />
          <img
            className={classes.Hidden}
            src={shape}
            width="100%"
            height="100%"
            alt="wishbirds.com"
          />
        </header>
      </div>
    </Fragment>
  );
};

export default Landing;
