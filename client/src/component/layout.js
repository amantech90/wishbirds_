import React ,{Fragment,Component} from 'react';



class Layout extends Component {
  render(){
    return(
      <Fragment>
             <main>
                  {this.props.children}
            </main>
          
      </Fragment>
    )
  }
} 

export default Layout;