module.exports =   {
  MongoDB : process.env.MONGO_DB_URI,
  secretKey : process.env.SECRET_KEY,
  cloudName : process.env.CLOUD_NAME,
  api_key: process.env.API_KEY, 
  api_secret: process.env.API_SECRET
}