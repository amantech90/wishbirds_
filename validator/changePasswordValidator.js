const validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function changePasswordValidator(data) {
  let errors = {};

  data.password = !isEmpty(data.password) ? data.password : '';
  data.password1 = !isEmpty(data.password1) ? data.password1 : '';
  data.password2 = !isEmpty(data.password2) ? data.password2 : '';

  if (validator.isEmpty(data.password)) {
    errors.password = 'password field is required';
  }



  if (!validator.isLength(data.password1, { min: 6, max: 12 })) {
    errors.password1 = 'password should be in 6 characters';
  }
  if (validator.isEmpty(data.password1)) {
    errors.password1 = 'password field is required';
  }
  if (!validator.equals(data.password1, data.password2)) {
    errors.password2 = 'password must be matched';
  }
  if (validator.isEmpty(data.password2)) {
    errors.password2 = 'confirm password field is required';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
