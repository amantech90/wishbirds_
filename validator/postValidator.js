const validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function postValidator(data) {
  let errors = {};

  data.wish = !isEmpty(data.wish) ? data.wish : '';
  data.name = !isEmpty(data.name) ? data.name : '';
  data.email = !isEmpty(data.wish) ? data.email : '';


  if (validator.isEmpty(data.wish)) {
    errors.wish = 'wish field is required';
  }

  if (!validator.isLength(data.name, { min: 2, max: 30 })) {
    errors.name = 'Name must in between 2 and 30 characters';
  }

  if (validator.isEmpty(data.name)) {
    errors.name = 'name field is required';
  }

  if (!validator.isEmail(data.email)) {
    errors.email = 'Invalid email address';
  }

  if (validator.isEmpty(data.email)) {
    errors.email = 'email field is required';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
