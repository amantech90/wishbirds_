const validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function EmailValidatorr(data) {
  let errors = {};
  data.email1 = !isEmpty(data.email1) ? data.email1 : '';
  if (!validator.isEmail(data.email1)) {
    errors.email1 = 'Invalid email address';
  }
  if (validator.isEmpty(data.email1)) {
    errors.email1 = 'email field is required';
  }
  return {
    errors,
    isValid: isEmpty(errors)
  };
};
