const validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function eventValider(data) {
  let errors = {};

  data.name = !isEmpty(data.name) ? data.name : '';
  data.desc = !isEmpty(data.desc) ? data.desc : '';
  data.image = !isEmpty(data.image) ? data.image : '';


  if (!validator.isLength(data.name, { min: 2, max: 30 })) {
    errors.name = 'Name must in between 2 and 30 characters';
  }

  if (validator.isEmpty(data.name)) {
    errors.name = 'name field is required';
  }

  if (!validator.isLength(data.desc, { min: 1, max: 140 })) {
    errors.desc = 'Description should be under 140 characters';
  }
  if (validator.isEmpty(data.desc)) {
    errors.desc = 'description field is required';
  }
  if (validator.isEmpty(data.image)) {
    errors.image = 'image is required';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
