const validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function registerValider(data) {
  let errors = {};

  data.name = !isEmpty(data.name) ? data.name : '';
  data.email = !isEmpty(data.email) ? data.email : '';
  data.password = !isEmpty(data.password) ? data.password : '';
  data.password2 = !isEmpty(data.password2) ? data.password2 : '';

  if (!validator.isLength(data.name, { min: 2, max: 30 })) {
    errors.name = 'Name must in between 2 and 30 characters';
  }

  if (validator.isEmpty(data.name)) {
    errors.name = 'name field is required';
  }

  if (!validator.isEmail(data.email)) {
    errors.email = 'Invalid email address';
  }

  if (validator.isEmpty(data.email)) {
    errors.email = 'email field is required';
  }
  if (!validator.isLength(data.password, { min: 6, max: 12 })) {
    errors.password = 'password should be in 6 characters';
  }
  if (validator.isEmpty(data.password)) {
    errors.password = 'password field is required';
  }
  if (!validator.equals(data.password, data.password2)) {
    errors.password2 = 'password must be matched';
  }
  if (validator.isEmpty(data.password2)) {
    errors.password2 = 'confirm password field is required';
  }



  return {
    errors,
    isValid: isEmpty(errors)
  };
};
