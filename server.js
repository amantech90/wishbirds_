const express = require('express');
const app = express();
const passport = require('passport');
const port = process.env.PORT || 5000;
const path = require('path');
const Mongoose = require('mongoose');
const db = require('./config/db').MongoDB
const bodyParser = require('body-parser');

const users = require('./routes/api/users');
const interns = require('./routes/api/intern');
const event = require('./routes/api/event');
const posts = require('./routes/api/post');
const admin = require('./routes/api/admin');


app.use(function(req, res, next) { res.header("Access-Control-Allow-Origin", "*"); res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept"); next(); });

Mongoose.connect(db).then(() => console.log('connected to the database')).catch(err=>{console.log(err)})
//passport middleware
app.use(passport.initialize());


require('./config/passport')(passport)
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

// Use Routes
app.use('/api/users', users);
app.use('/api/interns', interns);
app.use('/api/event', event);
app.use('/api/posts', posts);
app.use('/api/admin', admin);

// Server static assets if in production

  // Set static folder
  app.use(express.static('client/build'));

  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });


app.listen(port , ()=>{console.log(`Server has been started on port ${port}`)})